#!/bin/sh

mkdir -p openresty/nginx/logs

(
    touch openresty/nginx/logs/access.log
    touch openresty/nginx/logs/error.log

    tail -qF -n 0 openresty/nginx/logs/*.log
) &

(
	erb nginx.conf.erb > "openresty/nginx/conf/nginx.conf"
    echo 'Starting nginx'
	LD_LIBRARY_PATH="openresty/luajit/lib" openresty/nginx/sbin/nginx -p openresty/nginx
)
